import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:zengoquiz/zengoquiz.dart';

class StartModule extends StatefulWidget {
  const StartModule({Key? key}) : super(key: key);

  @override
  State<StartModule> createState() => _StartModuleState();
}

class _StartModuleState extends State<StartModule> {
  bool isFileLoaded = false;
  late File imageFile;
  late File noImage;
  late File quizSuccess;
  late File quizFail;
  late String questionsJson;

  Future<void> getImageFileFromAssets() async {
    final byteData1 = await rootBundle.load('assets/quizPicture.png');
    final file1 =
        File('${(await getTemporaryDirectory()).path}/quizPicture.png');
    await file1.writeAsBytes(byteData1.buffer
        .asUint8List(byteData1.offsetInBytes, byteData1.lengthInBytes));

    questionsJson = await rootBundle.loadString('assets/sampleJson.json');

    final byteData2 = await rootBundle.load('assets/no_image.jpg');
    final file2 = File('${(await getTemporaryDirectory()).path}/no_image.jpg');
    await file2.writeAsBytes(byteData2.buffer
        .asUint8List(byteData2.offsetInBytes, byteData2.lengthInBytes));

    final byteData3 = await rootBundle.load('assets/success.png');
    final file3 = File('${(await getTemporaryDirectory()).path}/success.png');
    await file3.writeAsBytes(byteData3.buffer
        .asUint8List(byteData3.offsetInBytes, byteData3.lengthInBytes));

    final byteData4 = await rootBundle.load('assets/fail.jpg');
    final file4 = File('${(await getTemporaryDirectory()).path}/fail.jpg');
    await file4.writeAsBytes(byteData4.buffer
        .asUint8List(byteData4.offsetInBytes, byteData4.lengthInBytes));

    setState(() {
      isFileLoaded = true;
      imageFile = file1;
      noImage = file2;
      quizSuccess = file3;
      quizFail = file4;
    });
  }

  @override
  void initState() {
    getImageFileFromAssets();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isFileLoaded
            ? ZengoQuiz(
                onResult: (value) {
                  print(value);
                },
                quizName: "Kvíz játékról",
                quizPicture: imageFile,
                questionNoImage: noImage,
                quizSuccessImage: quizSuccess,
                quizFailImage: quizFail,
                quizDescription:
                    "Válaszolj helyesen a kérdésekre, hogy megkapd a maximális pontszámot. Lorem ipsum dolor sit amet, consectetur adipiscing elit pharetra vitae tellus elit. ",
                quizTotalPoints: 30,
                questionsJson: questionsJson,
                appBarColor: Colors.white,
                appBarTitleColor: Colors.black,
                titleColor: Colors.black,
                textColor: Colors.black,
                buttonColor: Colors.orange,
                disabledButtonColor: Colors.grey,
                activeButtonColor: Colors.orange,
                highlightColor: Colors.orange,
              )
            : const CircularProgressIndicator());
  }
}
